import logging

import pytest
from gnar_gear import GnarApp
from unittest.mock import Mock

from piste.test.constants import ENVIRONMENT

log = logging.getLogger()


class TestApis:

    @pytest.fixture
    def mock_app(self, monkeypatch):
        monkeypatch.setattr('argparse.ArgumentParser.parse_args',
                            lambda *a, **kw: type('args', (object,), {'port': ''}))
        monkeypatch.setattr('flask.Flask.run', lambda *a, **kw: None)
        monkeypatch.setattr('os.environ', ENVIRONMENT)
        monkeypatch.setattr('postgres.Postgres.__init__', lambda *a, **kw: None)
        app = GnarApp('piste', production=False, port=9400)
        app.run()
        yield app.flask.test_client()

    def test_10_45(self, monkeypatch, mock_app):
        mock = Mock(return_value=type('response', (object,), {'json': lambda: 'Off-Piste checking in!'}))
        with monkeypatch.context() as m:
            m.setattr('requests.get', mock)
            mock_app.get('/10-45')
            mock.assert_called_with('http://127.0.0.1:9401/check-in', None)
